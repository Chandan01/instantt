package Instantt_project;

import static org.junit.Assert.fail;
import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Notification {
	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
		driver = new ChromeDriver();
		baseUrl = "https://devm1.instantt.co/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@Test
	public void testAddmerchant() throws Exception {
		File src = new File(
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/addrole.xls");
		FileInputStream fis = new FileInputStream(src);
		HSSFWorkbook wb = new HSSFWorkbook(fis);
		HSSFSheet sh1 = wb.getSheetAt(0);
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();		
		driver.findElement(By.id("username")).sendKeys(sh1.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.id("login")).clear();		
		driver.findElement(By.id("login")).sendKeys(sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.cssSelector("input.form-button")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Notification")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Broadcast Notification")).click();
		Thread.sleep(3000);
		String value = driver.findElement(By.xpath("//*[@id='notification_form']/div/div[1]/title")).getText();
		
		if(value.equals("Broadcast Message")){
			System.out.println("Pass");			
		}else{
			System.out.println("Fail");
		}
		driver.findElement(By.linkText("Log Out")).click();	
	    driver.quit();
	}
	
	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
