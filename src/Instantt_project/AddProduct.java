package Instantt_project;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AddProduct {
	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
		driver = new ChromeDriver();
		baseUrl = "https://devm1.instantt.co/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testAddProduct() throws Exception {
		File src = new File(
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/addrole.xls");
		FileInputStream fis = new FileInputStream(src);
		HSSFWorkbook wb = new HSSFWorkbook(fis);
		HSSFSheet sh1 = wb.getSheetAt(0);
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendKeys(
				sh1.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys(
				sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.cssSelector("input.form-button")).click();
		Thread.sleep(4000);
		driver.findElement(By.linkText("Catalog")).click();
		Thread.sleep(4000);
		driver.findElement(By.linkText("Manage Products")).click();
		driver.findElement(
				By.xpath("//div[@class='content-header']/descendant::button[contains(@class, 'scalable add')]"))
				.click();
		Thread.sleep(3000);		
		driver.findElement(By.xpath("//div/table/tbody/tr[3]/td/span/button"))
				.click();
		Thread.sleep(3000);
		driver.findElement(By.id("name")).clear();
		driver.findElement(By.id("name")).sendKeys(sh1.getRow(16).getCell(1).getStringCellValue());
		driver.findElement(By.id("description")).clear();
		driver.findElement(By.id("description")).sendKeys(sh1.getRow(17).getCell(1).getStringCellValue());
		driver.findElement(By.id("short_description")).clear();
		driver.findElement(By.id("short_description")).sendKeys(sh1.getRow(18).getCell(1).getStringCellValue());
		driver.findElement(By.id("sku")).clear();
		driver.findElement(By.id("sku")).sendKeys(sh1.getRow(19).getCell(1).getStringCellValue());
		new Select(driver.findElement(By.id("status")))
				.selectByVisibleText("Enabled");
		new Select(driver.findElement(By.id("visibility")))
				.selectByVisibleText("Catalog");
		driver.findElement(By.linkText("Prices")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.id("price")).clear();
		String Price = Integer.toString((int) sh1.getRow(20).getCell(1).getNumericCellValue());
		driver.findElement(By.id("price")).sendKeys(Price);
		new Select(driver.findElement(By.id("tax_class_id")))
				.selectByVisibleText("Taxable Goods");
		driver.findElement(
				By.cssSelector("#product_info_tabs_inventory > span")).click();
		driver.findElement(
				By.cssSelector("#product_info_tabs_categories > span")).click();
		driver.findElement(By.id("ext-gen236")).click();
		Thread.sleep(4000);
		driver.findElement(By.id("ext-gen344")).click();
		driver.findElement(By.id("ext-gen352")).click();
		driver.findElement(
				By.cssSelector("#product_info_tabs_customer_options > span"))
				.click();
		Thread.sleep(3000);
		driver.findElement(By.id("add_new_defined_option")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.id("product_option_1_title")).clear();
		driver.findElement(By.id("product_option_1_title")).sendKeys(sh1.getRow(21).getCell(1).getStringCellValue());
		new Select(driver.findElement(By.id("product_option_1_type")))
				.selectByVisibleText("Radio Buttons");
		driver.findElement(By.id("add_select_row_button_1")).click();
		driver.findElement(By.id("product_option_1_select_0_title")).clear();
		driver.findElement(By.id("product_option_1_select_0_title")).sendKeys(sh1.getRow(22).getCell(1).getStringCellValue());
		driver.findElement(By.id("product_option_1_select_0_price")).clear();
		String Price1 = Integer.toString((int) sh1.getRow(23).getCell(1).getNumericCellValue());
		driver.findElement(By.id("product_option_1_select_0_price")).sendKeys(Price1);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[@title='Save']")).click();
		Thread.sleep(3000);
		driver.close();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}
