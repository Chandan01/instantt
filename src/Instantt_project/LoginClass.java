package Instantt_project;

import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginClass {
	WebDriver driver;
	String baseUrl;
	StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
		driver = new ChromeDriver();
		baseUrl = "https://devm1.instantt.co/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@Test
	public void merchantLogin() throws Exception {
		//Login into Merchant panel 
		
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendKeys("mosaico");
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys("abcd@1234");
		driver.findElement(By.cssSelector("input.form-button")).click();
		driver.findElement(By.linkText("Log Out")).click();
		
	}
	
	@Test
	public void adminLogin() throws Exception {
		//Login into Admin panel	
		
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendKeys("admin");
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys("instantt@2016");
		driver.findElement(By.cssSelector("input.form-button")).click();
		driver.findElement(By.linkText("Log Out")).click();
				
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}
