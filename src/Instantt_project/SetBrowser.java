package Instantt_project;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SetBrowser {

	static WebDriver driver;

	public static WebDriver getBrowser(String browser) throws Exception {

		if (browser.equalsIgnoreCase("firefox")) {

			// create firefox instance

			driver = new FirefoxDriver();

		}

		// Check if parameter passed as 'chrome'

		else if (browser.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver",
					"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
			// create chrome instance

			driver = new ChromeDriver();

		}

		return driver;
	}
}