package Instantt_project;

import static org.junit.Assert.fail;
import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AddMerchant {
	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
		driver = new ChromeDriver();
		baseUrl = "https://devm1.instantt.co/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testAddmerchant() throws Exception {
		File src = new File(
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/addrole.xls");
		FileInputStream fis = new FileInputStream(src);
		HSSFWorkbook wb = new HSSFWorkbook(fis);
		HSSFSheet sh1 = wb.getSheetAt(0);
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();		
		driver.findElement(By.id("username")).sendKeys(sh1.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.id("login")).clear();		
		driver.findElement(By.id("login")).sendKeys(sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.cssSelector("input.form-button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@class='active']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='nav']/li[10]/a/span")).click();
		Thread.sleep(6000);
		driver.findElement(By.xpath("//*[@id='nav']/li[10]/ul/li[1]/a/span")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div[2]/div/div[2]/table/tbody/tr/td[2]/button"))
					.click();
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendKeys(sh1.getRow(4).getCell(1).getStringCellValue());
		driver.findElement(By.id("firstname")).clear();
		driver.findElement(By.id("firstname")).sendKeys(sh1.getRow(5).getCell(1).getStringCellValue());
		driver.findElement(By.id("lastname")).clear();
		driver.findElement(By.id("lastname")).sendKeys(sh1.getRow(6).getCell(1).getStringCellValue());
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email"))
				.sendKeys(sh1.getRow(7).getCell(1).getStringCellValue());
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(sh1.getRow(8).getCell(1).getStringCellValue());
		driver.findElement(By.id("confirmation")).clear();
		driver.findElement(By.id("confirmation")).sendKeys(sh1.getRow(9).getCell(1).getStringCellValue());
		new Select(driver.findElement(By.id("status")))
				.selectByVisibleText("Enabled");
		Thread.sleep(3000);
		driver.findElement(By.linkText("Merchant Address")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		driver.findElement(By.id("address1")).clear();
		driver.findElement(By.id("address1")).sendKeys(sh1.getRow(10).getCell(1).getStringCellValue());
		driver.findElement(By.id("postcode")).clear();
		String postcode = Integer.toString((int) sh1.getRow(11).getCell(1).getNumericCellValue());
		driver.findElement(By.id("postcode")).sendKeys(postcode);
		driver.findElement(By.id("city")).clear();
		driver.findElement(By.id("city")).sendKeys(sh1.getRow(12).getCell(1).getStringCellValue());
		driver.findElement(By.id("country")).clear();
		driver.findElement(By.id("country")).sendKeys(sh1.getRow(13).getCell(1).getStringCellValue());
		driver.findElement(By.linkText("User Role")).sendKeys(Keys.ENTER);
		driver.findElement(By.cssSelector("#Merchant_tabs_form_section_user_role > span"))
				.click();
		new Select(driver.findElement(By.name("limit")))
				.selectByVisibleText("200");
		driver.findElement(By.xpath("(//input[@name='roles[]'])[42]")).click();
		driver.findElement(By.linkText("Other Information")).sendKeys(Keys.ENTER);
		driver.findElement(By.id("shop_name")).clear();
		driver.findElement(By.id("shop_name")).sendKeys(sh1.getRow(14).getCell(1).getStringCellValue());
		driver.findElement(By.id("shop_tagline")).clear();
		driver.findElement(By.id("shop_tagline")).sendKeys(sh1.getRow(15).getCell(1).getStringCellValue());
		driver.findElement(By.xpath("//button[@title='Save Merchant']")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Log Out")).click();
		
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}
