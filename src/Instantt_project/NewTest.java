package Instantt_project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class NewTest {
	private WebDriver driver;
	/*private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();*/

	@BeforeClass
	@Parameters("browser")
	public void setUp(String browser) throws Exception {
		/*
		 * try { driver = SetBrowser.getBrowser(browser); baseUrl =
		 * "https://devm1.instantt.co/";
		 * driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); }
		 * catch (Exception e) { e.printStackTrace(); }
		 */
		if (browser.equalsIgnoreCase("firefox")) {

			driver = new FirefoxDriver();

		} else if (browser.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver",
					"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");

			driver = new ChromeDriver();

		} else {

			// If no browser passed throw exception

			throw new Exception("Browser is not correct");

		}

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void testAddiCredits() throws InterruptedException {
		File src = new File(
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/addrole.xls");
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(src);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HSSFWorkbook wb = null;
		try {
			wb = new HSSFWorkbook(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HSSFSheet sh1 = wb.getSheetAt(0);
		driver.get("https://devm1.instantt.co/index.php/admin");
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendKeys(
				sh1.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys(
				sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.cssSelector("input.form-button")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[6]/a/span")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[6]/ul/li[5]/a/span"))
				.click();
		driver.findElement(By.id("credit_amount")).clear();
		driver.findElement(By.id("credit_amount")).sendKeys("5");
		driver.findElement(By.id("comments")).clear();
		driver.findElement(By.id("comments")).sendKeys("Test iCredits");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String value = driver.findElement(
				By.xpath("//*[@id='messages']/ul/li/ul/li/span")).getText();

		if (value.equals("Credit is added successfully.")) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
		driver.findElement(By.linkText("Log Out")).click();
	}

	/*@AfterTest
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}*/

}
