package Instantt_project;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddiCredits {
	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver",
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
		driver = new ChromeDriver();
		baseUrl = "https://devm1.instantt.co/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testAddiCredits() throws Exception {
		File src = new File(
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/addrole.xls");
		FileInputStream fis = new FileInputStream(src);
		HSSFWorkbook wb = new HSSFWorkbook(fis);
		HSSFSheet sh1 = wb.getSheetAt(0);
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendKeys(
				sh1.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys(
				sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.cssSelector("input.form-button")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[6]/a/span")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[6]/ul/li[5]/a/span"))
				.click();
		driver.findElement(By.id("credit_amount")).clear();
		driver.findElement(By.id("credit_amount")).sendKeys("5");
		driver.findElement(By.id("comments")).clear();
		driver.findElement(By.id("comments")).sendKeys("Test iCredits");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		String value = driver.findElement(
				By.xpath("//*[@id='messages']/ul/li/ul/li/span")).getText();

		if (value.equals("Credit is added successfully.")) {
			System.out.println("Pass");
		} else {
			System.out.println("Fail");
		}
		driver.findElement(By.linkText("Log Out")).click();
	}

	@After
	public void tearDown() throws Exception {
		try {
			Thread.sleep(5000);
			driver.quit();
		} catch (Exception e) {
		}
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
