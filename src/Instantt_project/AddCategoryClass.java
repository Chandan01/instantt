package Instantt_project;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddCategoryClass {
	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
		driver = new ChromeDriver();
		baseUrl = "https://devm1.instantt.co/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void AddCategory() throws Exception {
		File src=new File("/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/addrole.xls");
		FileInputStream fis=new FileInputStream(src);
		HSSFWorkbook wb=new HSSFWorkbook(fis);
		HSSFSheet sh1= wb.getSheetAt(0);
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();		
		driver.findElement(By.id("username")).sendKeys(sh1.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.id("login")).clear();		
		driver.findElement(By.id("login")).sendKeys(sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.cssSelector("input.form-button")).click();	
		driver.findElement(By.xpath("//ul[@id='nav']/li[4]/a/span")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[4]/ul/li[2]/a/span")).click();
		Thread.sleep(3000);
	    driver.findElement(By.id("extdd-2")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.id("add_subcategory_button")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.id("group_4name")).clear();
	    driver.findElement(By.id("group_4name")).sendKeys(sh1.getRow(3).getCell(1).getStringCellValue());
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("//div[@class='content-header']/div/p/button[2]")).click();
		Thread.sleep(3000);
	   	driver.findElement(By.linkText("Log Out")).click();
	}

	@After
	public void tearDown() throws Exception {
		 try
	        {
	            Thread.sleep(5000);
	            driver.quit();
	        }
	        catch(Exception e)
	        {
	        }
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}