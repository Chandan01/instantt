package Instantt_project;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddRoleClass {
	private WebDriver driver;
	private String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",
				"/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/chromedriver");
		driver = new ChromeDriver();
		baseUrl = "https://devm1.instantt.co/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void AddRole() throws Exception {
		File src=new File("/home/inno/Uma_Backup/Instantt Selenium Script/Instantt/addrole.xls");
		FileInputStream fis=new FileInputStream(src);
		HSSFWorkbook wb=new HSSFWorkbook(fis);
		HSSFSheet sh1= wb.getSheetAt(0);			
		driver.get(baseUrl + "admin");
		driver.findElement(By.id("username")).clear();		
		driver.findElement(By.id("username")).sendKeys(sh1.getRow(0).getCell(1).getStringCellValue());
		driver.findElement(By.id("login")).clear();		
		driver.findElement(By.id("login")).sendKeys(sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.className("form-button")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[13]/a/span")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[13]/ul/li[11]/a/span")).click();
		driver.findElement(By.xpath("//ul[@id='nav']/li[13]/ul/li[11]/ul/li[2]/a/span"))
				.click();
		driver.findElement(By.cssSelector("button.scalable.add")).click();		
		driver.findElement(By.id("role_name")).clear();		
		driver.findElement(By.id("role_name")).sendKeys(sh1.getRow(2).getCell(1).getStringCellValue());		
		driver.findElement(By.id("current_password")).clear();		
		driver.findElement(By.id("current_password")).sendKeys(sh1.getRow(1).getCell(1).getStringCellValue());
		driver.findElement(By.cssSelector("#role_info_tabs_account > span"))
				.click();
		driver.findElement(By.cssSelector("#role_info_tabs_advanced > span"))
				.click();
		driver.findElement(By.id("access_scope_store")).click();
		driver.findElement(By.id("storetoggler1")).click();
		driver.findElement(By.id("ext-gen1968")).click();
		driver.findElement(By.xpath("//button[@title='Save Role']")).click();
		driver.findElement(By.linkText("Log Out")).click();
		
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}
}